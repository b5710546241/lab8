package pizzashop.food;

public abstract class AbstractItem implements OrderItem{
	protected int size;
	protected static final String [] sizes = { "None", "Small", "Medium", "Large" };
	
	public AbstractItem(int size){
		this.size = size;
	}
	
	public String getSize(){
		if(size>3 || size<0)
			return null;
		else
			return sizes[size];
	}
	
	public double getPrice(OrderItem item) {
		double price = 0;
		if ( size >= 0 && size < item.getPrices().length ) price = item.getPrices()[size];
		if (item instanceof Pizza) {
			for(Topping t: ((Pizza) item).getToppings()) {
				price += t.getPrice();
			}
		}
		return price;
	}
}
