package pizzashop.food;

public interface OrderItem {
	String getSize();
	double getPrice();
	double[] getPrices();
}
