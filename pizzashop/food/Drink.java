package pizzashop.food;

/** 
 * A drink has a size and flavor.
 */
public class Drink extends AbstractItem {
	// constants related to drink size
	public static final double [] prices = { 0.0, 20.0, 30.0, 40.0 };
	public static final String [] sizes = { "None", "Small", "Medium", "Large" };
	private String flavor;
	/**
	 * create a new drink.
	 * @param size is the size. 1=small, 2=medium, 3=large
	 */
	public Drink( int size ) {
		super(size);
	}
	
	/* 
	 * 
	 */
	public String toString() {
		return sizes[size] + " " + (flavor==null? "": flavor.toString()) + " drink"; 
	}

	/** return the price of a drink
	 * @see pizzashop.FoodItem#getPrice()
	 */
	public double getPrice() {
		return super.getPrice(this);
	}
	
	public Object clone() {
		Drink clone;
		
		try {
			clone = (Drink) super.clone();
		} catch (CloneNotSupportedException e) {
			System.err.println("Drink.clone: " + e);
			clone = new Drink(0);
		}
		//superclass already cloned the size and flavor
		return clone;
	}
	
	public double[] getPrices() {
		return prices;
	}

}
